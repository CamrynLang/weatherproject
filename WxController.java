/*
 * WxController
 * This is the controller for the FXML document that contains the view. 
 */
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author Tim McGowen
 */
public class WxController implements Initializable {

  @FXML
  private Button btnGetWx;

  @FXML
  private TextField txtZipcode;

  @FXML
  private Label lblCityState;

  @FXML
  private Label lblTime;

  @FXML
  private Label lblWeather;

  @FXML
  private Label lblTemperature;

  @FXML
  private Label lblWind;

  @FXML
  private Label lblPressure;

  @FXML
  private Label lblVisibility;

  @FXML
  private Label lblHumidity;

  @FXML
  private ImageView iconWx;

  @FXML
  private void handleButtonAction(ActionEvent e) {
    // Create object to access the Model
    WxModel weather = new WxModel();

    // Get zipcode
    String zipcode = txtZipcode.getText();

    // Use the model to get the weather information
    if (weather.isValid(zipcode) == true)
    {
      lblCityState.setText(weather.location);
      lblTime.setText(weather.time);
      lblWeather.setText(weather.weather);
      lblTemperature.setText(weather.temperature);
      lblWind.setText(weather.wind);
      lblPressure.setText(weather.pressure);
      lblHumidity.setText(weather.humidity);
      lblVisibility.setText(weather.windDirection);
      iconWx.setImage(weather.getImage());
    }
    else
    {
      lblCityState.setText("Invalid Zipcode");
      lblTime.setText("");
      lblWeather.setText("");
      lblTemperature.setText("");
      lblWind.setText("");
      lblPressure.setText("");
      lblVisibility.setText("");
      lblHumidity.setText("");
      iconWx.setImage(new Image("badzipcode.png"));
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    // TODO
  }    

}
