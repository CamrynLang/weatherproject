import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import javafx.scene.image.Image;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

//Camryn Langdon

public class WxModel {
    private static JsonElement jse;
    private static String ACCESS_TOKEN = "728cff123447ef2e39dc9c491721f146";

    public String location;
    public String time;
    public String weather;
    public String temperature;
    public String wind;
    public String pressure;
    public String windDirection;
    public String humidity;



    public boolean printWeather(String zipcode)
    {


        try
        {

            //http://api.openweathermap.org/data/2.5/weather?zip=95677&appid=728cff123447ef2e39dc9c491721f146
            String encodedURL = URLEncoder.encode(zipcode, "utf-8");
            URL weather = new URL("http://api.openweathermap.org/data/2.5/weather?zip=" + zipcode + "&units=imperial&appid=" + ACCESS_TOKEN);
            InputStream is = weather.openStream();// throws an IOException
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            jse = new JsonParser().parse(br);

                JsonElement location = jse.getAsJsonObject().get("name");
                this.location = String.valueOf(location).replaceAll("^\"|\"$", "");

                JsonElement date = jse.getAsJsonObject().get("dt");
                this.time = dateConversion(String.valueOf(date));

                JsonElement weatherDes = jse.getAsJsonObject().get("weather").getAsJsonArray().get(0).getAsJsonObject().get("description");
                this.weather = String.valueOf(weatherDes).replaceAll("^\"|\"$", "");

                JsonElement temperature = jse.getAsJsonObject().get("main").getAsJsonObject().get("temp");
                this.temperature = String.format("%.1f", Double.valueOf(String.valueOf(temperature)));

                JsonElement wind = jse.getAsJsonObject().get("wind").getAsJsonObject().get("speed");
                this.wind = wind + " MPH";

                JsonElement windDirection = jse.getAsJsonObject().get("wind").getAsJsonObject().get("deg");
                this.windDirection = " " + compass(String.valueOf(windDirection));

                JsonElement pressure = jse.getAsJsonObject().get("main").getAsJsonObject().get("pressure");
                this.pressure = convertFromHpaToHgInch(String.valueOf(pressure));

                JsonElement humidity = jse.getAsJsonObject().get("main").getAsJsonObject().get("humidity");
                this.humidity = " " + String.format("%.0f", Double.valueOf(String.valueOf(temperature))) + "%";


            is.close();
            br.close();
        }
        catch (java.io.UnsupportedEncodingException uee)
        {
            uee.printStackTrace();
        }
        catch (java.io.FileNotFoundException uee)
        {
            return false;
        }
        catch (java.net.MalformedURLException mue)
        {
            mue.printStackTrace();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }

        if(jse.getAsJsonObject().get("cod").getAsString() == "404") {
            return false;
        }

        return true;
    }


    public boolean isValid(String zipcode){
        return printWeather(zipcode);
        }

    public Image getImage()
    {
        String iconURL = jse.getAsJsonObject().get("weather").getAsJsonArray().get(0).getAsJsonObject().get("icon").getAsString();
        return new Image("http://openweathermap.org/img/wn/" + iconURL + "@2x.png");
    }

    private static String dateConversion(String seconds){
        long unixSeconds = Long.valueOf(String.valueOf(seconds));
        Date date = new Date(unixSeconds*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = sdf.format(date);
        return String.valueOf(formattedDate);
    }

    private static String convertFromHpaToHgInch(String hpa){
        String hg = String.format("%.2f",Double.valueOf(hpa) / 33.8638866667);
        return (hg);
    }

    private String compass(String input){
            Double degrees = Double.valueOf(input);
        if (degrees >= 331 || degrees <= 29)  return "N" ;
        if (degrees >= 30 || degrees <= 60)  return "NE" ;
        if (degrees >= 61 || degrees <= 120)  return "E" ;
        if (degrees >= 121 || degrees <= 150)  return "SE" ;
        if (degrees >= 151 || degrees <= 210)  return "S" ;
        if (degrees >= 211 || degrees <= 239)  return "SW" ;
        if (degrees >= 240 || degrees <= 300)  return "W" ;
        if (degrees >= 301 || degrees <= 330)  return "NW" ;
        return "";
    }
    public static void main(String[] args) {
        WxModel temp = new WxModel();
        temp.isValid("95677");
        System.out.println(temp.getImage());
        //System.out.println(String.format("%.0f", 10.2 ));
    }

}
